import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Lists {
	
	ArrayList<String> al;
	
	void example(){
		al = new ArrayList<String>(2);
		al.add("asdf");
		al.add("zxcv1");
		al.ensureCapacity(4);
		al.addAll(al);
		
		strLenComp sLenDist = new strLenComp();
		Collections.sort(al, sLenDist);
		print();
		//asdf
		//asdf
		//zxcv1
		//zxcv1
	}
	
	void print(){
		for(String str : al)
			System.out.println(str);
	}
	
	class strLenComp implements Comparator<String>{
		@Override
		public int compare(String o1, String o2) {
			int l1 = o1.length();
			int l2 = o2.length();
			
			if (l1<l2)
				return -1;
			else if (l1==l2)
				return 0;
			else
				return 1;
		}
		
	}
}

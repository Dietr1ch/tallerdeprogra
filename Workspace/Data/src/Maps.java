import java.util.HashMap;
import java.util.Map;


public class Maps {
	
	HashMap<Integer, Double> intToDouble;
	
	void example(){
		intToDouble = new HashMap<Integer, Double>();
		
		intToDouble.put(1,1.0);
		intToDouble.put(-2,Math.PI);
		intToDouble.put(2012,Math.E);
		
		System.out.println(intToDouble.get(2012));
		//E
		intToDouble.remove(2012);
		intToDouble.put(1,10.0);
		
		for(Map.Entry<Integer, Double> entry : intToDouble.entrySet()){
			System.out.print(entry.getKey());
			System.out.print("->");
			System.out.println(entry.getValue());
		}
		//1->10.0
		//-2->PI
	}
}

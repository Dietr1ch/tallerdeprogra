import java.awt.geom.Point2D;

@SuppressWarnings("serial")//you don't need this, do you?
public class Point extends Point2D.Double{
	
	public Point(double x, double y){
		super(x,y);
	}
	
	public Point PolarPoint(double r, double tetha, Point ref){
		double X = r*Math.cos(tetha);
		double Y = r*Math.sin(tetha);
		return new Point(X+ref.x,Y+ref.y);
	}
	
	public double angle(Point ref){
		return Math.atan2(ref.y-y, ref.x-x);
	}
	public double radius(Point ref){
		return Point2D.distance(x,y, ref.x, ref.y);
	}
}

class Pair {
	int a, b;

	public Pair(int A, int B) {
		// if(B<A){int t=A;A=B;B=t;}
		a = A;
		b = B;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;

		Pair p = (Pair) obj;
		return a == p.a && b == p.b;
	}

	@Override
	public int hashCode() {
		String str = a + "|" + b;
		return str.hashCode();
	}
}
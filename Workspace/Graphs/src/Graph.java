import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public class Graph {
	
	HashMap<Integer, Vertex> vertices;
	HashMap<Pair, Edge> edges;
	
	HashMap<Integer, ArrayList<Integer>> out;
	HashMap<Integer, ArrayList<Integer>> in;
	
	public Graph(){
		vertices = new HashMap<Integer, Vertex>();
		edges = new HashMap<Pair, Edge>();
		out = new HashMap<Integer, ArrayList<Integer>>();
		in = new HashMap<Integer, ArrayList<Integer>>();
	}
	public Graph(int vInitialCap, int eInitialCap){
		vertices = new HashMap<Integer, Vertex>(vInitialCap);
		edges = new HashMap<Pair, Edge>(eInitialCap);
		out = new HashMap<Integer, ArrayList<Integer>>(eInitialCap);
		in = new HashMap<Integer, ArrayList<Integer>>(eInitialCap);
	}

	
	void addVertex(int id){
		Vertex v = new Vertex(id);
		//if(vertices.containsKey(id)) return;
		vertices.put(id, v);
	}
	void removeVertex(int id){
		if(vertices.remove(id)==null)
			return;
		
		ArrayList<Integer> List = out.get(id);
		if(List!=null)
			for(Integer dest : List)
				removeEdge(id,dest);
		
		List = in.get(id);
		if(List!=null)
			for(Integer src : List)
				removeEdge(id,src);
	}
	
	void addEdge(int id1, int id2){
		Edge e = new Edge();
		Pair p = new Pair(id1,id2);
		edges.put(p, e);
		
		ArrayList<Integer> outList = out.get(id1);
		if(outList == null)
			outList = new ArrayList<Integer>();
		outList.add(id2);
		out.put(id1,outList);
		
		
		ArrayList<Integer> inList = in.get(id2);
		if(inList == null)
			inList = new ArrayList<Integer>();
		inList.add(id1);
		in.put(id2,inList);
	}
	
	void removeEdge(int a, int b){
		Pair p = new Pair(a, b);
		edges.remove(p);
	}
	
	
}


public class StringBuilderEg {
	
	static String repeat(char c, int times){
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < times; i++)
			s.append(c);
		return s.toString();
	}
	
	static String reverse(String str){
		StringBuilder s = new StringBuilder(str);
		s.reverse();
		return s.toString();
	}

}

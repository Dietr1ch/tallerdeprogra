
public class OutputFormat {
	
	void printf(){
		System.out.format(
				"signed decimal integer: %+d\n", -42);
		//signed decimal integer: -42
		System.out.format(
				"unsigned octal integer: %o\n", 65);
		//unsigned octal integer: 101
		System.out.format(
				"unsigned hexadecimal integer: %#x, %X\n", 31, 63);
		//unsigned hexadecimal integer: 0x1f, 3F
		System.out.format(
				"real number, standard notation: %2.3f\n", 100*Math.PI);
		//real number, standard notation: 314.159
		System.out.format(
				"real number, scientific notation: %.3e %.9E\n", Math.E, Math.E);
		//real number, scientific notation: 2.718e+00 2.718281828E+00
		System.out.format(
				"%%f or %%e: %+g %G\n", Math.PI/100000.0, 1.0);
		//%f or %e: +3.14159e-05 1.00000
		System.out.format(
				"String:\n" +
				"-%10s-\n" +
				"-%-10s-\n", "hola", "chao");
		//String:
		//-      hola-
		//-chao      -
		System.out.format(
				"Character: %c", 67);
		//Character: C
	}
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class tsp {
		
	public static void main(String[] args) throws IOException {

		int[][] inc;
		int[] grado;
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bf = new BufferedReader(isr);
		String s;
		
		while(!(s = bf.readLine()).equals("0 0")) {
		
			String arr[] = s.split(" ");
			int N = Integer.parseInt(arr[0]);
			int C = Integer.parseInt(arr[1]);
			
			inc = new int[C][N];
			grado = new int[C];
			
			// crea la matriz de incidencia
			for(int i = 0; i <= C-1; i++)
			{
				String arr2[] = bf.readLine().split(" ");
				int n = Integer.parseInt(arr2[0]);
				
				for(int j = 1; j <= n ; j++)
				{
					int k = Integer.parseInt(arr2[j]);
					inc[i][k] = 1;
				}
			}
			
			// repara aristas sobre el mismo nodo (color) cambiando 1 por 2
			int cuenta = 0;
			int indice = 0;
			for (int j = 0; j < N; j++) // chequea columna por columna
			{
				cuenta = 0;
				for (int i = 0; i <= C-1; i++)
				{	
					cuenta += inc[i][j];
					if ( inc[i][j] == 1 ) indice = i;
				} 
				
				if (cuenta == 1) inc[indice][j] = 2;
			}

			
			// si el grafo no tiene ni ciclo ni camino Euleriano imprime -1
			if ( !Euleriano(inc,C,N) )
				System.out.println("-1");
			else
			{	
				// busca los nodos (colores) de grado impar, si es que los hay
				int A = -1;
				int B = -1;
				
				// calcula los grados y encuentra los de grado impar
				for(int i = 0; i < C; i++)
				{
					for(int j = 0; j < N; j++)
						grado[i] += inc[i][j];
					
					if (A == -1) 
					{
						if (grado[i]%2 == 1) A = i;
					}
					else
					{
						if (grado[i]%2 == 1) B = i;
					}
				}

				// si no hay nodos de grado impar, hay un ciclo Euleriano que empieza en cualquier nodo
				// luego el menor valor es 0
				if ( A == -1 && B == -1)
					System.out.println("0");
				else
				{
					// ahora sabemos que el camino Euleriano, es entre A y B
					// encontramos la menor arista desde A y B en el camino Euleriano
					int aristaA = encuentraArista(inc,C,N,A);
					int aristaB = encuentraArista(inc,C,N,B);
				 
				 	if (aristaA < aristaB) System.out.println(aristaA);
					else System.out.println(aristaB);
				}
			}
						
		}
	}
	
	public static boolean Euleriano(int[][] inc, int C, int N)
	{
		// primero chequea que tenga a lo mas dos nodos de grado impar
		int cuenta = 0;
		int grado;
		
		for(int i = 0; i < C; i++)
		{
			grado = 0;
			for(int j = 0; j < N; j++)
				grado += inc[i][j];
			if (grado%2 == 1) cuenta++;
		}
		
		if (cuenta > 2) return false;
		
		
		// ahora chequea que sea conexo (entre los nodos no aislados)
		// primero calcula los nodos alcanzables (desde un nodo no aislado)
		int[] alcanzables = new int[C];
		alcanza(inc,C,N,alcanzables);
		
		// chequea que los no alcanzables sean todos aislados
		for(int i = 0; i < C; i++)
		{
			if (alcanzables[i] == 0)
			{
				for (int j = 0; j < N; j++)
				{
					if (inc[i][j] != 0) return false;
				}
			}	
		}
		
		return true;
	}
	
	public static void alcanza(int[][] inc, int C, int N, int[] alcanzados)
	{
		// busca el primer nodo no aislado
		boolean encontrado = false;
		int A = 0;
		
		for(int i = 0; i < C && !encontrado; i++)
			for(int j = 0; j < N && !encontrado; j++)
				if(inc[i][j] != 0) 
				{ 
					A = i;
					encontrado = true;
				}
			
		// ahora marca y expande ese nodo
		alcanzados[A] = 1;
		expande(inc,C,N,alcanzados,A);
	}
	
	public static void expande(int[][] inc, int C, int N, int[] alcanzados, int A)
	{
		// expande cada vecino de A (si no esta alcanzado)
		for(int j = 0; j < N; j++)
			if (inc[A][j] == 1)
			{
				// ahora busca el vecino
				for(int i = 0; i < C; i++)
				{
					if(inc[i][j] == 1 && i != A && alcanzados[i] != 1)
					{
						alcanzados[i] = 1;
						expande(inc,C,N,alcanzados, i);
					}
				}
			}
	}
	
	public static int encuentraArista(int[][] inc, int C, int N, int A)
	{	
		boolean encontrado = false;
		int aristaA = -2;
		int[][] inct;
		
		// chequea una a una las aristas que inciden en A
		for(int n = 0; n < N && !encontrado; n++)
		{
			if (inc[A][n] != 0) 					
			{
				// copia inc a inct
				inct =  new int[C][N];
				for(int i = 0; i < C; i++)
					for(int j = 0; j < N; j++)
						inct[i][j] = inc[i][j];
										
				// borra la columna correspondiente a n (elimina la arista)
				for(int i = 0; i < C; i++)
					inct[i][n] = 0;
				
				// determina si aun el grafo es Euleriano
				if ( Euleriano(inct,C,N) )
				{
					encontrado = true;
					aristaA = n;
				}
			}
		}
		
		return aristaA;
	}

}


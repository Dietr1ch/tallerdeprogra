import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class candy {
		
	public static void main(String[] args) throws IOException {
	
		int[] linea;
		int[] columna;

	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bf = new BufferedReader(isr);
		String s;
		
		while(!(s = bf.readLine()).equals("0 0")) {
		
			String arr[] = s.split(" ");
			int M = Integer.parseInt(arr[0]);
			int N = Integer.parseInt(arr[1]);
			
			linea = new int[N];
			columna = new int[M];
			
			for(int i = 0; i <= M-1; i++)
			{
				String arr2[] = bf.readLine().split(" ");
				
				for(int j = 0; j <= N-1; j++)
					linea[j] = Integer.parseInt(arr2[j]);
					
				int t = total(linea, N-1);
				columna[i] = t;
			}
			
			System.out.println(total(columna, M-1));
		}
	}

	public static int total(int[] lin, int L)
	{
		if (L == 0) return lin[0];
		if (L == 1 && lin[0] > lin[1]) return lin[0];
		if (L == 1 && lin[1] > lin[0]) return lin[1];
		if (L >= 2)
		{
			for( int i = 2; i <= L; i++ )
			{
				if ( lin[i]+lin[i-2] > lin[i-1] ) lin[i] = lin[i]+lin[i-2];
				else lin[i] = lin[i-1];
			}
		}
		return lin[L];
	}
}


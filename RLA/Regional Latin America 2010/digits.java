import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class digits {
	
	public static int[] digit = {0,0,0,0,0,0,0,0,0,0};
	
	public static void main(String[] args) throws IOException {
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bf = new BufferedReader(isr);
		String s;
		
		while(!(s = bf.readLine()).equals("0 0")) {
		
			String[] arr = s.split(" ");
			int a = Integer.parseInt(arr[0]);
			int b = Integer.parseInt(arr[1]);
			
			for (int d = 0; d <= 9; d++)
			{
				digit[d] = 0;
			}

		    cuenta(a, arr[0].length()-1, b, arr[1].length()-1);	
			
			for (int i= 0; i<10; i++){
				System.out.print(digit[i]);
				if (i != 9)
					System.out.print(" ");
				else
					System.out.println("");
			}
		}
	}
	
	// funcion para contar entre a y b (n y m son los exponentes de los digitos mas significativos en base 10)
	public static void cuenta(int a, int n, int b, int m)
	{
		int dn = (int)Math.pow(10,n); // 10^n
		int dm = (int)Math.pow(10,m); // 10^m

		if ( n < m ) // si n < m reduce el problema a uno en donde los argumentos sean iguales
		{
			cuenta(a, n , dm-1, m-1);
			cuenta(dm, m, b, m); 
		}	
		else 
		{
			if (n==0) // si hay un digito simplemente cuenta
			{
				for (int i = a; i<= b; i++)
				{
					digit[i]++;
				}
			}
			else // si hay mas de un digito separa para contar
			{ 
				// separa a y b				
				int msa = (int) (a/dn);
				int msb = (int) (b/dn);
			
				int ra = a % dn;
				int rb = b % dn;
				
				if ( msa < msb )
				{
					// cuenta entre a y (msa+1)*10^n - 1
					digit[msa]+= dn - ra;
					cuenta(ra, n-1, dn-1, n-1);
				
					// cuenta entre (msb)*10^n y b
					digit[msb] += b % dn + 1;				
					cuenta(0, n-1, rb, n-1);
				
					// cuenta todos los otros digitos 
					// (iterando sobre la posicion mas significativa)
					for (int i = msa+1; i < msb; i++)
					{
					    // digitos en la posicion mas significativa
				 		digit[i] += dn;
						
						// luego digitos en otras posiciones
						for(int d = 0; d <= 9; d++)
						{
							digit[d] += n*Math.pow(10,n-1);
						}
					}
				}
				
				if ( msa == msb ) // cuidado en la cuenta!!
				{
					digit[msa] += rb - ra + 1;
					cuenta(ra, n-1, rb, n-1);
				}	
			}	
		}
	}
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class cocircular {
	
	// punto[i][0] : coordenada x del punto i
	// punto[i][1] : coordenada y del punto i
	public static int[][] punto;
	
	public static void main(String[] args) throws IOException {
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bf = new BufferedReader(isr);
		String s;
		
		while(!(s = bf.readLine()).equals("0")) {
		
			int N = Integer.parseInt(s);
			punto = new int[N][2];
			
			for(int i = 0; i < N; i++)
			{
				String arr[] = bf.readLine().split(" ");
				punto[i][0] = Integer.parseInt(arr[0]);
				punto[i][1] = Integer.parseInt(arr[1]);
			}
			
			int max = 2;
			int[] p1;
			int[] p2;
			int[] p3;
			int[] p4;
			
			for(int i=0; i<N; i++)
			{
				for(int j=i+1; j<N; j++)
				{
					for(int k=j+1; k<N; k++)
					{
						int M = 0;
						
						p1 = punto[i];
						p2 = punto[j];
						p3 = punto[k];
						
						if (!colin(p1,p2,p3))
						{
							for(int l=0; l<N; l++)
							{
								p4 = punto[l];
								if( cocirc(p1,p2,p3,p4) ) M++;
							}
						}
						
						if ( M > max ) max = M;
					}
				}
			}
			
			System.out.println(max);
 			
		}
	}
		
	// funciones auxiliares para simplificar los calculos
	public static long a(int[] p1, int[] p2)
	{
		return p1[0] + p2[0];
	}
	
	public static long b(int[] p1, int[] p2)
	{
		return p1[1] + p2[1];
	}
	
	public static long c(int[] p1, int[] p2)
	{
		return p1[0] - p2[0];
	}
	
	public static long d(int[] p1, int[] p2)
	{
		return p1[1] - p2[1];
	}
	
	// numerador coordenada x del centro del circulo que pasa por p1,p2,p3
	public static long r(int[] p1, int[] p2, int[] p3)
	{
		return 
		( 
			( a(p1,p3)*c(p1,p3) + b(p1,p3)*d(p1,p3) ) * d(p1,p2) 
			-
			( a(p1,p2)*c(p1,p2) + b(p1,p2)*d(p1,p2) ) * d(p1,p3)
		);
	}
	
	// denominador coordenada x del centro del circulo que pasa por p1,p2,p3
	public static long s(int[] p1, int[] p2, int[] p3)
	{
		return 
		( c(p1,p3)*d(p1,p2) - c(p1,p2)*d(p1,p3) )  
		;
	}
	
	// numerador coordenada y del centro del circulo que pasa por p1,p2,p3	
	public static long t(int[] p1, int[] p2, int[] p3)
	{
		return 
		( 
			( b(p1,p3)*d(p1,p3) + a(p1,p3)*c(p1,p3) ) * c(p1,p2) 
			-
			( b(p1,p2)*d(p1,p2) + a(p1,p2)*c(p1,p2) ) * c(p1,p3)
		);
	}

	// denominador coordenada y del centro del circulo que pasa por p1,p2,p3	
	public static long u(int[] p1, int[] p2, int[] p3)
	{
		return 
		( d(p1,p3)*c(p1,p2) - d(p1,p2)*c(p1,p3) )  
		;
	}

	// si algun numerador es 0 entonces los puntos son colineales
	public static boolean colin(int[] p1, int[] p2, int[] p3)
	{
		return s(p1,p2,p3) == 0 || u(p1,p2,p3) == 0;
	}
	
	// determina si los centros de los circulos definidos por p1,p2,p3 y p1,p2,p4 son iguales
	public static boolean cocirc(int[] p1, int[] p2, int[] p3, int[] p4)
	{
		return r(p1,p2,p3)*s(p1,p2,p4)==r(p1,p2,p4)*s(p1,p2,p3) && t(p1,p2,p3)*u(p1,p2,p4)==t(p1,p2,p4)*u(p1,p2,p3);
	}

}


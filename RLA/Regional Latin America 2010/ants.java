import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ants {
	
	public static int[] padre;
	public static long[] costo;
	
	public static void main(String[] args) throws IOException {
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bf = new BufferedReader(isr);
		String s;
		
		while(!(s = bf.readLine()).equals("0")) {
		
			int N = Integer.parseInt(s);
			padre = new int[N];
			costo = new long[N];
			
			for(int i = 1; i <= N-1; i++)
			{
				String arr[] = bf.readLine().split(" ");
				padre[i] = Integer.parseInt(arr[0]);
				costo[i] = Long.parseLong(arr[1]);
			}
			
			int Q = Integer.parseInt(bf.readLine());
			
			for(int i = 1; i <= Q; i++)
			{
				String arr[] = bf.readLine().split(" ");
				int S = Integer.parseInt(arr[0]);
				int T = Integer.parseInt(arr[1]);
				
				// hasta aqui solo leer input!
				
				if ( S >= T ) System.out.print(cuenta(S,T));
				else System.out.print(cuenta(T,S));
				
				if (i < Q) System.out.print(" ");
				else System.out.println();
			}
		}
	}
	
	// precondicion: A >= B
	public static long cuenta(int A, int B)
	{
		if (A == B) return 0;
		else 
		{
			if (padre[A] >= B) return costo[A] + cuenta(padre[A],B);
			else return costo[A] + cuenta(B,padre[A]);
		}
	}
	
}


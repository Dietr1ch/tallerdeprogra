import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class power{
	
	public static double distancia(int x1, int x2, int y1, int y2){
		return Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
	}
	
	static public class nodo{
		
		public int numerohijos;
		public nodo padre;
		public int x;
		public int y;
		public int power;
		
		public nodo(nodo p, int px, int py, int pw){
			padre = p;
			x = px;
			y = py;
			numerohijos = 0;
			power = pw;
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();
		int esp, N, C, x, y, power, ans;
		nodo[] planta;
		nodo father;
		
		while(!s.equals("0 0")){
			esp = s.indexOf(" ");
			N = Integer.parseInt(s.substring(0,esp));
			C = Integer.parseInt(s.substring(esp+1));
			
			planta = new nodo[N];
			
			s = br.readLine();
			esp = s.indexOf(" ");
			x = Integer.parseInt(s.substring(0,esp));
			s = s.substring(esp+1);
			esp = s.indexOf(" ");
			y = Integer.parseInt(s.substring(0,esp));
			power = Integer.parseInt(s.substring(esp+1));
			planta[0] = new nodo(null, x, y, power);
			
			double dist;
			
			for(int i=1; i<N; i++){
				s = br.readLine();
				esp = s.indexOf(" ");
				x = Integer.parseInt(s.substring(0,esp));
				s = s.substring(esp+1);
				esp = s.indexOf(" ");
				y = Integer.parseInt(s.substring(0,esp));
				power = Integer.parseInt(s.substring(esp+1));
				
				dist = 2000;
				father = null;
				
				for(int j=0; j<i; j++){
					if(distancia(x,planta[j].x,y,planta[j].y) < dist){
						dist = distancia(x,planta[j].x,y,planta[j].y);
						father = planta[j];
					}
				}
				
				father.numerohijos++;
				planta[i] = new nodo(father, x, y, power);
			}
			
			ans=0;
			boolean seguir = true;
			
			while(seguir){				
				for(int i=0; i<N; i++){
					if(planta[i]!= null && planta[i].numerohijos == 0){
						
						if(planta[i].power < C){
							if(planta[i].padre == null) seguir = false;
							else{
								planta[i].padre.power += planta[i].power;
								planta[i].padre.numerohijos--;
							}
						}
						else{
							ans++;
							if(planta[i].padre == null) seguir = false;
							else{
								planta[i].padre.numerohijos--;
							}
						}
						
						planta[i] = null;
					}
				}
			}
			
			System.out.println(ans);
			s = br.readLine();
			
		}
	}
}
import java.util.Scanner;
import java.util.Stack;



public class mine {
	public int x,y;
	public mine(int X, int Y){
		x=X;y=Y;
	}
	
	
	static Scanner s = new Scanner(System.in);
	
	static int R,C;
	
	public static void main(String[] zxcv){
		
		R=s.nextInt();
		C=s.nextInt();
		s.nextLine();//skip eol
		
		while(R!=0 && C!=0){
		
			read();
			solve();

			R=s.nextInt();
			C=s.nextInt();
			s.nextLine();//skip eol
		}
		
		s.close();
	}
	
	static byte[][] matrix;
	static Stack<mine> bombs;
	static void solve(){
		while(!bombs.empty())
			addNeightbourhood(bombs.pop());
		print();
	}
	
	static void addNeightbourhood(mine m){
		addNeightbourhood(m.x, m.y);
	}
	static void addNeightbourhood(int r, int c){
		int rmin=Math.max(r-1, 0);
		int rmax=Math.min(r+1, R-1);
		
		int cmin=Math.max(c-1, 0);
		int cmax=Math.min(c+1, C-1);
		
		for (int i = rmin; i <= rmax; i++)
			for (int j = cmin; j <= cmax; j++)
				matrix[i][j]++;
	}
	
	
	static void read(){
		matrix = new byte[R][C];
		bombs = new Stack<mine>();
		
		for (int i = 0; i < R; i++) {
			String line = s.nextLine();
			for (int j = 0; j < C; j++) {
				char c = line.charAt(j);
				byte val;
				switch (c) {
					case '*':
						val=-127; //-9 is enough
						mine m = new mine(i,j);
						bombs.push(m);
						break;
					default:
						val=0;
						break;
				}
				matrix[i][j]=val;				
			}
		}
	}
	
	static void print(){
		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				byte b = matrix[i][j];
				if(b>=0)
					System.out.print(b);
				else
					System.out.print('*');
			}
			System.out.println();
		}
	}
	
	
}

import java.util.Scanner;

public class condorcet {
	
	static Scanner s;
	
	static int candidatesCount;
	static int votesCount;
	static int[][] comparisons;
	
	public static void main(String[] zzzzzz) {
		s = new Scanner(System.in);
		
		votesCount = s.nextInt();
		candidatesCount = s.nextInt();
		
		int caseNumber=1;
		
		while (votesCount > 0 && candidatesCount > 0) {
			
			comparisons = new int[candidatesCount][];
			for (int i = 0; i < candidatesCount; i++)
				comparisons[i] = new int[candidatesCount - (1 + i)]; // Comparison[i][i+1..cC]
																															// =
																															// Comparison[i][0..cC-(i+1)]
			
			for (int i = 0; i < votesCount; i++)
				parseVote();
			
			
			
			System.out.print("Case "+caseNumber+++": ");
			findWinner();
			
			
			
			votesCount = s.nextInt();
			candidatesCount = s.nextInt();
		}
		
		s.close();
	}
	
	static void prefeerOver(int a, int b) {
		if (a < b)
			comparisons[a][b-(a+1)]++;
		else
			comparisons[b][a-(b+1)]--;
	}
	
	static void parseVote() {
		int[] votes = new int[candidatesCount];
		for (int i = 0; i < candidatesCount; i++)
			votes[i] = s.nextInt();
		
		for (int candI = 0; candI < votes.length - 1; candI++)
			for (int candJ = candI + 1; candJ < votes.length; candJ++)
				prefeerOver(votes[candI], votes[candJ]);
	}
	
	
	static void findWinner() {
		int winner = -1;
		int winnerCount = 0;
		
		for (int candI = 0; candI < comparisons.length && winnerCount < 2; candI++) {
			int nonPositives = 0;
			
			
			
			
			for (int prevCand = 0; prevCand < candI; prevCand++) {
				int vote = -comparisons[prevCand][candI - (prevCand + 1)];
				
				if (vote < 0) {
					nonPositives++;
					break;
				}
			}
			if (nonPositives > 0)
				continue;// no way
				
			
			
			
			for (int nextCand = candI + 1; nextCand < candidatesCount; nextCand++) {
				int vote = comparisons[candI][nextCand - (candI + 1)];
				
				if (vote < 0) {
					nonPositives++;
					break;
				}
			}
			
			
			//maybe
			if (nonPositives == 0) {
				winner = candI;
				winnerCount++;
			}
			
		}
		
		if (winnerCount == 1) {
			System.out.println(winner);
		}
		else
			System.out.println("No Condorcet winner");
	}
	
}

import java.util.HashMap;
import java.util.Scanner;



public class ripoff {
	
	static Scanner s = new Scanner(System.in);
	
	static int length,delta,turns;
	static int[] board;
	
	public static void main(String[] zz){
		
		length=s.nextInt();
		
		while(length!=0){
			delta=s.nextInt();
			turns=s.nextInt();
			length+=2;//shift ninja :X
			
			board = new int[length];
			for (int i = 1; i < board.length-1; i++)
				board[i]= s.nextInt();
			
			solve();
			
			length=s.nextInt();
		}
	}
	
	
	
	static HashMap<Integer, Integer> optimum;
	static void solve(){
		optimum = new HashMap<Integer, Integer>();
		
		for (int t = 0; t <= turns; t++){
			int key = enc(length-1,t);
			optimum.put(key, 0);
		}
			
		
		
		for (int pos = length-1; pos >= 0; pos--) {
			for (int t = 1; t <= turns; t++) {
				//At pos with t turns left.

				
				
				//Calc best move value
				int candVal = Integer.MIN_VALUE;
				
				for (int d = 1; d <= delta; d++) {
					//advance
					int candKey = enc(pos+d,t-1);
					Integer value = optimum.get(candKey);
					if (value==null)
						continue;
					
					candVal = Math.max(candVal,value);
				}
				
				if (candVal==Integer.MIN_VALUE)
					continue;
				
				//candVal gives the best income, so we take it
				int key = enc(pos,t);
				optimum.put(key, board[pos]+candVal);
				
			}
		}
		
		int bestKey = enc(0,turns);
		int bestVal = optimum.get(bestKey);
		
		System.out.println(bestVal);
	}
	
	static int enc(int pos, int turn){
		return 300*pos+turn;
	}
	
}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class voting {

	static InputStreamReader inp = new InputStreamReader(System.in);
  static BufferedReader br = new BufferedReader(inp);
	
	
	public static void main(String[]zzz) throws IOException{
		
		char vote=(char) br.read();
		
		while(vote!='#'){
			int yes=0;
			int no=0;
			int none=0;
			int absent=0;
			
			//read votes
			while(vote!='\n'){
				switch (vote) {
					case 'Y':
						yes++;
						break;
					case 'N':
						no++;
						break;
					case 'P':
						none++;
						break;
					case 'A':
						absent++;
						break;
				}
				vote=(char) br.read();
			}
			
			/*
			System.err.println("yes:"+yes);
			System.err.println("no:"+no);
			System.err.println("Null:"+none);
			System.err.println("absent:"+absent);
			*/
			int total = yes+no+none+absent;
			
			if (absent*2>=total)
				System.out.println("need quorum");
			else if (yes>no)
				System.out.println("yes");
			else if (yes==no)
				System.out.println("tie");
			else
				System.out.println("no");
			
			
			vote=(char) br.read();
		}
	}
}

import java.util.Arrays;
import java.util.Scanner;

public class hex {
	
	static Scanner s = new Scanner(System.in);
	
	static int[] nums = new int[12];
	
	public static void main(String[] zxcv) {
		readInput();
		while (endCond()) {
			solve();
			
			readInput();
		}
		
		s.close();
	}
	
	static int sum;
	static void readInput() {
		sum=0;
		for (int i = 0; i < 12; i++){
			int a = s.nextInt();
			nums[i] = a;
			sum+=a;
		}
		
		Arrays.sort(nums);
	}
	
	static boolean endCond() {
		for (int i = 0; i < 12; i++)
			if (nums[i] != 0)
				return true;
		return false;
	}
	
	static void solve() {
		
	}
	
}

import java.util.Scanner;

public class Main {
	
	static Scanner s;
	static int desktop = 1;
	
	public static void main(String[] asdf) {
		s = new Scanner(System.in);
		
		int rectCount = s.nextInt();
		
		while (rectCount != 0) {
			Rect[] rects = new Rect[rectCount];
			
			for (int i = 0; i < rectCount; i++) {
				int r = s.nextInt();
				int c = s.nextInt();
				int w = s.nextInt();
				int h = s.nextInt();
				
				rects[i] = new Rect(c, r, w, h);
			}
			
			System.out.println("Desktop " + (desktop++) + ":");
			
			int queries = s.nextInt();
			for (int i = 0; i < queries; i++) {
				
				int r = s.nextInt();
				int c = s.nextInt();
				
				int window = 0;
				for (int j = 0; j < rects.length; j++) {
					if (rects[j].isInside(c, r))
						window = j + 1;
				}
				if (window == 0)
					System.out.println("background");
				else
					System.out.println("window " + window);
			}
			
			rectCount = s.nextInt();
		}
		
	}
}

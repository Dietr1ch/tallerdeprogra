	public class Rect{
		public int X,Y,W,H;
		
		public Rect(int x, int y, int w, int h){
			X=x;
			Y=y;
			W=w;
			H=h;
		}
		
		public boolean isInside(int a, int b){
			return X<=a && a<X+W
					&& Y<=b && b<Y+H;
		}
	}
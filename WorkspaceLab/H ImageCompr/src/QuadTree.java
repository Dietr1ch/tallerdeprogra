import java.util.Scanner;

public class QuadTree {
	
	/**
	 * 1 2 3 4
	 */

	
	private QuadTree[] child = new QuadTree[4];
	private double color;
	
	public void setImg(){
		setImg(0,0,img.length);
	}
	public double setImg(int minX, int minY, int size) {
		if (size == 1) {
			color = img[minX][minY];
			return color;
		}
		
		size /= 2;
		for (int i = 0; i < child.length; i++)
			child[i] = new QuadTree();
		
		child[0].setImg(minX, minY, size);
		child[1].setImg(minX, minY + size, size);
		child[2].setImg(minX + size, minY, size);
		child[3].setImg(minX + size, minY + size, size);

		color=0;
		color+=child[0].color;
		color+=child[1].color;
		color+=child[2].color;
		color+=child[3].color;
		color /= 4;
		
		return color;
	}
	
	void paint(){
		paint(0,0,img.length);
	}
	
	void paint(int minX, int minY, int size) {
		if (size == 1) {
			img[minX][minY] = getColor(color);
		}
		
		int c = getColor(color);
		if(c!=-1){
			for (int i = minX; i < minX + size; i++)
				for (int j = minY; j < minY + size; j++)
					img[i][j] = c;
			return;
		}
		

		size /= 2;
		child[0].paint(minX, minY, size);
		child[1].paint(minX, minY + size, size);
		child[2].paint(minX + size, minY, size);
		child[3].paint(minX + size, minY + size, size);
		
	}
	
	
	static int getColor(double color){
		if(color>=threshold)
			return 1;
		else if(1-color>=threshold)
			return 0;
		return -1;
	}
	
	static Scanner s = new Scanner(System.in);
	static int w;
	static int[][] img;
	static double threshold;
	
	public static void main(String[] zzz) {
		w=s.nextInt();
		
		int caseNumber=1;
		
		
		while(w!=0){
			
			System.out.println("Image "+caseNumber+++":");
			
			threshold=s.nextInt();
			threshold/=100;
			img=new int[w][w];
			
			s.nextLine();
			for (int i = 0; i < w; i++) {
				String l = s.nextLine();
				for (int j = 0; j < w; j++) {
					img[i][j]=l.charAt(j)-'0';
				}
			}
			
			QuadTree Q = new QuadTree();
			Q.setImg();
			Q.paint();
			for (int i = 0; i < w; i++) {
				for (int j = 0; j < w; j++)
					System.out.print(img[i][j]);
				System.out.println();
			}
			
			w=s.nextInt();
		}
		
		s.close();
	}
}

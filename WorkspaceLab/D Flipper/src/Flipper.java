public class Flipper {
	
	int N;
	int L;
	int R;
	Card[] cards;
	public Card _base;
	
	public Flipper(int n) {
		N = n;
		L = 0;
		R = n - 1;
		cards = new Card[N];
		
		int number=1;
		for (int i = 0; i < N; i++, number++)
			cards[i]=new Card(number);
		
		
	}
	
	public void SetFaces(char[] faces) {
		if (faces.length != N)
			return;
		
		for (int i = 0; i < N; i++) {
			if (faces[i] == 'U')
				cards[i].isFacedUp = true;
			else
				cards[i].isFacedUp = false;
		}
	}
	
	public void ApplyFlips(char[] flips) {
		if (flips.length != N-1)
			return;
		int max = N - 1;
		for (int i = 0; i < max; i++) {
			char c = flips[i];
			if (c == 'L')
				leftFlip();
			else
				rightFlip();
		}
	}
	
	public void leftFlip() {
		if (L > R)
			return;
		
		Card _l = cards[L];
		L++;
		_base = cards[L];
		
		Card t = _l.getTop();
		_base.addFlip(t);
		
		t=t.bot;
		while(t!=null){
			_base.addFlip(t);
			t=t.bot;
		}
		
	}
	
	public void rightFlip() {
		if (L > R)
			return;
		
		Card _r = cards[R];
		R--;
		_base = cards[R];
		
		Card t = _r.getTop();
		_base.addFlip(t);
		
		t=t.bot;
		while(t!=null && _base!=t){
			_base.addFlip(t);
			t=t.bot;
		}

	}
}

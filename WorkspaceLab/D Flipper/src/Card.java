

public class Card {
	
	public int number;
	public Card top;
	public Card bot;
	
	public Boolean isFacedUp;
	
	public Card(int n){
		number = n;
	}
	
	public String toString(){
		return ""+number;
	}
	
	public Card getTop(){
		Card T = this;
		
		while(T.top!=null)
			T=top;
		
		return T;
	}
	
	public void addFlip(Card c){
		c.isFacedUp=!c.isFacedUp;//flip
		Card T=getTop();
		T.top=c;
		c.bot=T;
		c.top=null;
	}
	
	
}

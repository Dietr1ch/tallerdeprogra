import java.util.Scanner;

public class mein {

	static Scanner s;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		
		int N=s.nextInt();
		int CaseNumber=1;
		
		while(N!=0){
			s.nextLine();//skip int's line
			String orientation = s.nextLine();
			String flips = s.nextLine();
			
			char[] o = orientation.toCharArray();
			char[] f = flips.toCharArray();
			
			Flipper flipper = new Flipper(N);
			
			flipper.SetFaces(o);
			flipper.ApplyFlips(f);
			
			
			int[] cardNumber = new int[N];
			Boolean[] cardFacedTop = new Boolean[N];
			
			Card c = flipper._base;
			for (int i = 0; i < N; i++) {
				cardNumber[i]=c.number;
				cardFacedTop[i]=c.isFacedUp;
				c=c.top;
			}
			
			int Q = s.nextInt();
			
			System.out.println("Pile "+CaseNumber++);
			for (int i = 0; i < Q; i++) {
				int q = s.nextInt();
				int r = N-q;
				if(cardFacedTop[r])
					System.out.println("Card "+q+"is a face up "+cardNumber[r]+".");
				else
					System.out.println("Card "+q+"is a face down "+cardNumber[r]+".");
			}
			
			
			N=s.nextInt();
		}
		

		s.close();
	}

}

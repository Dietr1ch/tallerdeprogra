import java.util.Scanner;

public class fools {
	
	static Scanner S;
	static int s,c,p,l;
	
	public static void main(String[] zzzz){
		S = new Scanner(System.in);
		
		
		s=S.nextInt();
		c=S.nextInt();
		p=S.nextInt();
		l=S.nextInt();
		
		int caseNumber=1;
		
		while(s!=0){
			System.out.print("Case "+caseNumber+++": ");
			solve();
			
			
			s=S.nextInt();
			c=S.nextInt();
			p=S.nextInt();
			l=S.nextInt();
		}
		
		S.close();
	}
	
	static void solve(){
		int mcd = Mcd(s,c);
		
		
		if(mcd==1){//coprimos, tiene soluci�n
			sim();
		}
		else{ //puede tener soluci�n
			if((p-l)%mcd!=0)
				System.out.println("Never");
			else
				sim();
		}
		
		
	}
	
	
	static void sim(){
		
		int dientesF = s-p;
		int posC=(l+dientesF)%c;
		
		int revs = 0;
		while(posC!=0){
			posC=(posC+s)%c;
			revs++;
		}
		
		if(dientesF>=s){
			dientesF%=s;
			revs++;
		}
		
		System.out.println(revs+" "+dientesF+"/"+s);
	}
	
	
	
	public static int Mcd(int x, int y) {
		int temp;
		
		while (x > 0) {		
			if (x < y) {//swap
				temp = x;
				x = y;
				y = temp;
			}
			
			x-= y;
		}
		return y;
	}
		
}

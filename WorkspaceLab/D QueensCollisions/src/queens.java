import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;



public class queens {
	static Scanner S = new Scanner(System.in);
	static int n;
	
	
	static HashMap<Integer, Integer> horizontal;
	static HashMap<Integer, Integer> vertical;
	static 	HashMap<Integer, Integer> d1;
	static HashMap<Integer, Integer> d2;
	
	public static void main(String[] zzz){
		n = S.nextInt();
		
		while(n!=0){
			parse();
			solve();
			n=S.nextInt();
		}
		
		S.close();
	}
	
	
	static void parse(){
		int g = S.nextInt();
		
		horizontal = new HashMap<Integer, Integer>();
		vertical = new HashMap<Integer, Integer>();
		d1 = new HashMap<Integer, Integer>();
		d2 = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < g; i++) {
			int k = S.nextInt();
			int x = S.nextInt();
			int y = S.nextInt();
			int s = S.nextInt();
			int t = S.nextInt();
			
			for (int j = 0; j < k; j++) {
				
				Integer hVal = horizontal.get(x);
				if(hVal!=null)
					horizontal.put(x, hVal+1);
				else
					horizontal.put(x, 1);
				
				Integer vVal = vertical.get(y);
				if(vVal!=null)
					vertical.put(y, vVal+1);
				else
					vertical.put(y, 1);
				
				
				int d1Key = x-y;
				Integer d1Val = d1.get(d1Key);
				if(d1Val!=null)
					d1.put(d1Key, d1Val+1);
				else
					d1.put(d1Key, 1);
				
				
				int d2Key = x+y;
				Integer d2Val = d2.get(d2Key);
				if(d2Val!=null)
					d2.put(d2Key, d2Val+1);
				else
					d2.put(d2Key, 1);
				
				x+=s;
				y+=t;
			}
			
		}
		
		
		
	}
	
	static void solve(){
		int collisions=0;
		
    for(Entry<Integer, Integer> e :horizontal.entrySet()){
    	collisions+=e.getValue()-1;
    }
    
    for(Entry<Integer, Integer> e :vertical.entrySet()){
    	collisions+=e.getValue()-1;
    }
    for(Entry<Integer, Integer> e :d1.entrySet()){
    	collisions+=e.getValue()-1;
    }
    
    for(Entry<Integer, Integer> e :d2.entrySet()){
    	collisions+=e.getValue()-1;
    }
    
    
    System.out.println(collisions);
	}
	
	
}

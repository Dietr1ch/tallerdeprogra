import java.util.Scanner;



public class Blah {
	
	static int R,C;
	static char[][]board;
	static char[][]originalboard;
	static String command;
	
	
	static int row,col;
	
	static int testgame = 1;
	
	public static void main(String[] args) {
		Scanner s= new Scanner(System.in);
		
		R = s.nextInt();
		C = s.nextInt();
		s.nextLine();
		while(R!=0){
			
			board = new char[R][C];
			originalboard = new char[R][C];
			
			for (int i = 0; i < R; i++) {
				String line = s.nextLine();
				for (int j = 0; j < C; j++) {
					char ch = line.charAt(j);		
					
					board[i][j] = ch;		
					originalboard[i][j] = ch;
					
					if(ch=='w' || ch=='W'){
						row =i;
						col =j;
					}
						
					
					
				}
			}
			
			command = s.nextLine();
			
			solve();
			
			
			R = s.nextInt();
			C = s.nextInt();
			s.nextLine();
		}
		
		
		s.close();
	}
	
	static boolean ended=false ;
	
	static void solve(){
		ended=false;
		for (int i = 0; i < command.length(); i++) {
			move(command.charAt(i));	
			
			/*System.out.println(command.charAt(i));
			
			for (int i1 = 0; i1 < R; i1++) {
				String line ="";
				for (int j = 0; j < C; j++) {
					line = line + board[i1][j]+"";
					
				}
				System.out.println(line);
			}*/
			
			
			
			
			ended =checkComplete();		
			if(ended)
				break;
		}
		String resultado = "";
		if(ended)
			resultado="complete";
		else
			resultado = "incomplete";
		//Print
		System.out.println("Game "+(testgame++)+": "+resultado);
		
		for (int i = 0; i < R; i++) {
			String line ="";
			for (int j = 0; j < C; j++) {
				line = line + board[i][j]+"";
				
			}
			System.out.println(line);
		}
		
		
	}

	private static boolean checkComplete() {
		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				if(board[i][j]=='b' || board[i][j]=='+')
					return false;				
			}			
		}
		
		return true;
		
	}

	private static void move(char c) {
		
		int oldrow=row;
		int oldcol=col;
		boolean mayus = board[row][col]=='W';
		
		switch (c) {
			case 'U':
				if(board[row-1][col]=='.'){
					
					board[row][col] = '.';
					board[--row][col] ='w';					
				}
				else if(board[row-1][col]=='b' && board[row-2][col]=='.'){
					board[row-2][col] = 'b';
					board[row-1][col] = 'w';
					board[row--][col] ='.';					
				}	
				else if(board[row-1][col]=='B' && board[row-2][col]=='.'){
					board[row-2][col] = 'b';
					board[row-1][col] = 'W';
					board[row--][col] ='.';					
				}
				else if(board[row-1][col]=='+'){
					board[row][col] = '.';
					board[--row][col] ='W';					
				}
				else if(board[row-1][col]=='b' && board[row-2][col]=='+'){
					board[row-2][col] = 'B';
					board[row-1][col] = 'w';
					board[row--][col] ='.';					
				}
				else if(board[row-1][col]=='B' && board[row-2][col]=='+'){
					board[row-2][col] = 'B';
					board[row-1][col] = 'W';
					board[row--][col] ='.';					
				}
					
				break;
			case 'D':
				if(board[row+1][col]=='.'){
					board[row][col] = '.';
					board[++row][col] ='w';					
				}
				else if(board[row+1][col]=='b' && board[row+2][col]=='.'){
					board[row+2][col] = 'b';
					board[row+1][col] = 'w';
					board[row++][col] ='.';					
				}	
				else if(board[row+1][col]=='B' && board[row+2][col]=='.'){
					board[row+2][col] = 'b';
					board[row+1][col] = 'W';
					board[row++][col] ='.';					
				}
				else if(board[row+1][col]=='+'){
					board[row][col] = '.';
					board[++row][col] ='W';					
				}
				else if(board[row+1][col]=='b' && board[row+2][col]=='+'){
					board[row+2][col] = 'B';
					board[row+1][col] = 'w';
					board[row++][col] ='.';					
				}
				else if(board[row+1][col]=='B' && board[row+2][col]=='+'){
					board[row+2][col] = 'B';
					board[row+1][col] = 'W';
					board[row++][col] ='.';					
				}
				break;
			case 'L':
				if(board[row][col-1]=='.'){
					board[row][col] = '.';
					board[row][--col] ='w';					
				}
				else if(board[row][col-1]=='b' && board[row][col-2]=='.'){
					board[row][col-2] = 'b';
					board[row][col-1] = 'w';
					board[row][col--] ='.';					
				}	
				else if(board[row][col-1]=='B' && board[row][col-2]=='.'){
					board[row][col-2] = 'b';
					board[row][col-1] = 'W';
					board[row][col--] ='.';					
				}
				else if(board[row][col-1]=='+'){
					board[row][col] = '.';
					board[row][--col] ='W';					
				}
				else if(board[row][col-1]=='b' && board[row][col-2]=='+'){
					board[row][col-2] = 'B';
					board[row][col-1] = 'w';
					board[row][col--] ='.';					
				}
				else if(board[row][col-1]=='B' && board[row][col-2]=='+'){
					board[row][col-2] = 'B';
					board[row][col-1] = 'W';
					board[row][col--] ='.';					
				}
				break;
			case 'R':
				if(board[row][col+1]=='.'){
					board[row][col] = '.';
					board[row][++col] ='w';					
				}
				else if(board[row][col+1]=='b' && board[row][col+2]=='.'){
					board[row][col+2] = 'b';
					board[row][col+1] = 'w';
					board[row][col++] ='.';					
				}	
				else if(board[row][col+1]=='B' && board[row][col+2]=='.'){
					board[row][col+2] = 'b';
					board[row][col+1] = 'W';
					board[row][col++] ='.';					
				}
				else if(board[row][col+1]=='+'){
					board[row][col] = '.';
					board[row][++col] ='W';					
				}
				else if(board[row][col+1]=='b' && board[row][col+2]=='+'){
					board[row][col+2] = 'B';
					board[row][col+1] = 'w';
					board[row][col++] ='.';					
				}
				else if(board[row][col+1]=='B' && board[row][col+2]=='+'){
					board[row][col+2] = 'B';
					board[row][col+1] = 'W';
					board[row][col++] ='.';					
				}
				break;	
			default:
				break;
		}
		
		if(mayus){
			board[oldrow][oldcol]='+';
		}
		
		
	}
	
}

import java.util.ArrayList;
import java.util.Scanner;

public class crash {
	
	static Scanner s = new Scanner(System.in);
	static int martiansCount = 0;
	
	public static double dist(double ax, double ay, double bx, double by) {
		double dx = (ax - bx);
		double dy = (ay - by);
		
		double d2 = dx * dx + dy * dy;
		return Math.sqrt(d2);
	}
	
	public static void main(String[] zzz) {
		
		martiansCount = s.nextInt();
		
		while (martiansCount > 0) {
			solve();
			martiansCount = s.nextInt();
		}
		
		s.close();
	}
	
	static ArrayList<Martian> marts;
	static void solve() {
		marts = new ArrayList<Martian>(martiansCount);
		for (int i = 0; i < martiansCount; i++) {
			double x = s.nextInt();
			double y = s.nextInt();
			double r = s.nextInt();
			Martian m = new Martian(x, y, r);
			merge(m);
		}
		
		System.out.println(marts.size());
	}

	
	
	static void merge(Martian m) {
		
		ArrayList<Martian> farMarts= new ArrayList<Martian>(martiansCount);
		ArrayList<Martian> nearMarts= new ArrayList<Martian>(martiansCount);
		
		
		for(Martian m2 : marts)
			if(m.inRange(m2))
				nearMarts.add(m2);
			else
				farMarts.add(m2);
		

		nearMarts.add(m);
		Martian group = fusion(nearMarts);
		
		if (nearMarts.size()>1){
			marts=farMarts;
			merge(group);
			return;
		}
		
		farMarts.add(m);
		marts=farMarts;
	}
	
	static Martian fusion(ArrayList<Martian> nearMarts){
		double X=0;
		double Y=0;
		double R=0;
		
		if (nearMarts.isEmpty())
			return null;
		
		for (Martian nm : nearMarts) {
			X+=nm.posX;
			Y+=nm.posY;
			R+=nm.radio*nm.radio;
		}
		
		X/=nearMarts.size();
		Y/=nearMarts.size();
		R=Math.sqrt(R);
		
		return new Martian(X,Y,R);
	}
	
}


public class Martian {
	
	public double posX, posY, radio;
	
	public Martian(double x, double y, double r) {
		posX = x;
		posY = y;
		radio = r;
	}
	
	double dist(Martian m) {
		return crash.dist(posX, posY, m.posX, m.posY);
	}
	
	boolean inRange(Martian m) {
		double mDist = dist(m);
		return mDist <= Math.max(radio, m.radio);
	}
	/*
	void join(Martian m) {
		if (inRange(m)) {
			posX = (posX + m.posX) / 2;
			posY = (posY + m.posY) / 2;
			
			radio*=radio;
			radio+=m.radio*m.radio;
			radio=Math.sqrt(radio);
		}
	}
	*/
	
	
}
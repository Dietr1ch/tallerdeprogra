import java.util.ArrayList;
import java.util.Scanner;

public class mein {
	
	static Scanner s;
	static long a, b;
	
	public static void main(String[] zzz) {
		s = new Scanner(System.in);
		
		a = s.nextInt();
		b = s.nextInt();
		
		long caseNumber = 1;
		
		while (a != 0 && b != 0) {
			
			System.out.println("Problem " + caseNumber++);
			solve();
			
			a = s.nextInt();
			b = s.nextInt();
		}
		
		s.close();
	}
	
	static String sw_Mult;
	
	static void solve() {
		sw_Mult = a * b + "";
		System.out.println(strLength(a + ""));
		System.out.println(strLength(b + ""));
		makeDash();
		makeNums();
		System.out.println(sw_Mult);
		
	}
	
	static void makeNums() {
		char[] nums = (b + "").toCharArray();
		ArrayList<String> mults = new ArrayList<String>();
		
		for (int i = nums.length - 1; i >= 0; i--) {
			long d = Long.parseLong(nums[i] + "");
			String m = strMult(a, d, nums.length - i-1-trails.length());
			if (m != null)
				mults.add(m);
		}
		
		if (mults.size() > 1) {
			for (String str : mults)
				System.out.println(str);
			makeDash();
		}
		
	}
	
	static String trails = "";
	static String strMult(long a, long d, long i) {
		if (d == 0) {
			trails += "0";
			return null;
		}
		
		String sw_d = a * d + trails;
		trails = "";
		
		return strLength(sw_d, sw_Mult.length()-i);
	}
	
	static void makeDash() {
		System.out.println(repeat('-', sw_Mult.length()));
	}
	
	static String strLength(String s) {
		return strLength(s, sw_Mult.length());
	}
	
	static String strLength(String s, long minLength) {
		long delta = minLength - s.length();
		if (delta < 0)
			delta = 0;
		
		String str = repeat(' ', delta);
		
		return str + s;
	}
	
	static String repeat(char c, long length) {
		StringBuilder buf = new StringBuilder();
		while (length-- > 0)
			buf.append(c);
		return buf.toString();
	}
	
}

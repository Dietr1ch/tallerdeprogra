
public class GridSquare {
	
	static int vamps = 1;
	
	public enum type {
		blank, mirror, vampire, mortal
	};
	
	public enum direction {
		noDir, E, N, S, W
	};
	
	public type T = type.blank;
	public direction D = direction.noDir;
	
	int id;
	
	public GridSquare() {
	};
	
	public GridSquare(type t, direction d) {
		T = t;
		if (T == type.vampire)
			id = vamps++;
		else if (T == type.mirror)
			D = d;
	}
	
}

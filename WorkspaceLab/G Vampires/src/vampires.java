import java.awt.Point;
import java.util.Scanner;

public class vampires {
	
	static Scanner s;
	
	static int vampireCount;
	static int mortalsCount;
	static int mirrorsCount;
	
	public static void main(String[] asdf) throws Exception {
		
		s = new Scanner(System.in);
		
		vampireCount = s.nextInt();
		mortalsCount = s.nextInt();
		mirrorsCount = s.nextInt();
		
		int caseNum = 1;
		
		while (!(vampireCount == 0 && mortalsCount == 0 && mirrorsCount == 0)) {
			
			System.out.println("Case " + caseNum + ":");
			caseNum++;
			solveCase();
			
			vampireCount = s.nextInt();
			mortalsCount = s.nextInt();
			mirrorsCount = s.nextInt();
		}
		
		s.close();
		
	}
	
	static void solveCase() {
		
		// init
		GridSquare[][] grid = new GridSquare[101][101];
		for (int i = 0; i < 101; i++)
			for (int j = 0; j < 101; j++)
				grid[i][j] = new GridSquare();
		
		Point[] vamps = new Point[vampireCount];
		
		// Parse vamps
		for (int i = 0; i < vampireCount; i++) {
			Point p = new Point();
			p.x = s.nextInt();
			p.y = s.nextInt();
			vamps[i] = p;
			
			grid[p.x][p.y].T = GridSquare.type.vampire;
		}
		
		// Parse mortals
		for (int i = 0; i < mortalsCount; i++) {
			int morX = s.nextInt();
			int morY = s.nextInt();
			
			grid[morX][morY].T = GridSquare.type.mortal;
		}
		
		s.nextLine();
		// parse mirrors
		for (int i = 0; i < mirrorsCount; i++) {
			
			String line = s.nextLine();
			Scanner sc = new Scanner(line);
			char c = sc.findInLine(".").charAt(0);
			line = sc.nextLine();
			
			sc = new Scanner(line);
			
			int x1 = sc.nextInt();
			int y1 = sc.nextInt();
			int x2 = sc.nextInt();
			int y2 = sc.nextInt();
						

			int minX=Math.min(x1,x2);
			int minY=Math.min(y1,y2);
			int maxX=Math.max(x1,x2);
			int maxY=Math.max(y1,y2);
			
			
			for (int mirrorX = minX; mirrorX <= maxX; mirrorX++) {
				for (int mirrorY = minY; mirrorY <= maxY; mirrorY++) {
					grid[mirrorX][mirrorY].T = GridSquare.type.mirror;
					
					switch (c) {
						case 'E':
							grid[mirrorX][mirrorY].D = GridSquare.direction.E;
							break;
						case 'N':
							grid[mirrorX][mirrorY].D = GridSquare.direction.N;
							break;
						case 'S':
							grid[mirrorX][mirrorY].D = GridSquare.direction.S;
							break;
						case 'W':
							grid[mirrorX][mirrorY].D = GridSquare.direction.W;
							break;
						default:
							break;
					}
					
				}
			}
			
		}// end mirrors
		
		int embarrasedVamps = 0;
		// solve
		for (int i = 0; i < vamps.length; i++) {
			int X = vamps[i].x;
			int Y = vamps[i].y;
			
			boolean E = false;
			boolean N = false;
			boolean S = false;
			boolean W = false;
			
			// left (W)
			for (int dx = X; dx >= 0; dx--) {
				
				GridSquare local = grid[dx][Y];
				
				if (local.T == GridSquare.type.mortal)// collision with mortal
					break;
				if (local.T == GridSquare.type.mirror) {// collision with mirror
					// reflection
					W = (local.D == GridSquare.direction.E);
					break;
				}
			}
			
			// right (E);
			for (int dx = X; dx < 101; dx++) {
				
				GridSquare local = grid[dx][Y];
				
				if (local.T == GridSquare.type.mortal)// collision with mortal
					break;
				if (local.T == GridSquare.type.mirror) {// collision with mirror
					// reflection
					E = (local.D == GridSquare.direction.W);
					break;
				}
			}
			
			// up (N);
			for (int dy = Y; dy < 101; dy++) {
				
				GridSquare local = grid[X][dy];
				
				if (local.T == GridSquare.type.mortal)// collision with mortal
					break;
				if (local.T == GridSquare.type.mirror) {// collision with mirror
					// reflection
					N = (local.D == GridSquare.direction.S);
					break;
				}
			}
			
			// down (S);
			for (int dy = Y; dy >= 0; dy--) {
				
				GridSquare local = grid[X][dy];
				
				if (local.T == GridSquare.type.mortal)// collision with mortal
					break;
				if (local.T == GridSquare.type.mirror) {// collision with mirror
					// reflection
					S = (local.D == GridSquare.direction.N);
					break;
				}
			}
			
			if (E || N || S || W) {
				System.out.print("vampire " + (i + 1));
				if (E)
					System.out.print(" east");
				if (N)
					System.out.print(" north");
				if (S)
					System.out.print(" south");
				if (W)
					System.out.print(" west");
				System.out.println();
				embarrasedVamps++;
			}
			
		}// end single vamp test
		
		if (embarrasedVamps == 0)
			System.out.println("none");
		
	}
	
}

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Pizza implements Comparable{
	
	static Scanner s;
	static int pizzaCount;
	
	int diameter;
	int price;
	
	public Pizza(int d, int p) {
				diameter=d;
				price =p;
	}
	
	double Area(){
		//return Math.PI*(diameter/2)*(diameter/2);//pi R*R
		return (diameter/2)*(diameter/2);//pi R*R
	}
	double AreaRatio(){
		return Area()/(double)price;
	}

	
	public static void main(String[] zzz) {
		s = new Scanner(System.in);
		
		pizzaCount = s.nextInt();
		
		int caseNumber = 1;
		
		while (pizzaCount != 0) {
			System.out.print("Menu " + caseNumber+++": ");
			solve();
			
			pizzaCount = s.nextInt();
		}
		
		s.close();
	}
	
	static void solve() {
		Pizza[] pizzasList = new Pizza[pizzaCount];
		
		for (int i = 0; i < pizzaCount; i++)
			pizzasList[i]=new Pizza(s.nextInt(),s.nextInt());
		
		Arrays.sort(pizzasList);
		System.out.println(pizzasList[0].diameter);
	}
	
	

	public int compareTo(Object p) {
		Pizza P = (Pizza) p;
		if (AreaRatio()>P.AreaRatio())
			return -1;
		else
			return 1;
	}
	
	
}

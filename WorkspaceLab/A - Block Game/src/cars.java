import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class cars {
	
	int w, h;
	char color;
	
	public boolean isVertical() {
		return w == 1;
	}
	
	public cars(int W, int H, char Color) {
		w = W;
		h = H;
		color=Color;
	}
	
	static Scanner s = new Scanner(System.in);
	static char car;
	static ArrayList<cars> blocks;
	static HashMap<cars, IntPoint2D> carPos;
	
	public static void main(String[] zxcvzxcv) {
		
		car = s.nextLine().charAt(0);
		while (car != '*') {
			
			read();
			solve();
			car = s.nextLine().charAt(0);
		}
		s.close();
	}
	
	static void solve() {
		
	}
	
	static HashSet<Character> colors;
	
	static void read() {
		blocks = new ArrayList<cars>();
		colors = new HashSet<Character>();
		
		char[][] initial = new char[6][6];
		for (int i = 0; i < 6; i++) {
			String line = s.nextLine();
			for (int j = 0; j < 6; j++) {
				char c = line.charAt(j);
				initial[i][j] = c;
				if (c != '.')
					colors.add(c);
			}
		}
		
		Iterator<Character> itr = colors.iterator();
		while (itr.hasNext()) {
			char c = itr.next();
			
			int i = 0, j = 0;
			boolean ok = false;
			for (; i < 6 && !ok; i++)
				for (j=0; j < 6 && !ok; j++) {
					if (initial[i][j] == c) {
						
						int h = 0, w = 0;
						for (h = i; h < 6; h++)
							if (initial[h][j] != c)
								break;
						for (w = j; w < 6; w++)
							if (initial[i][w] != c)
								break;
						
						h -= i;
						w -= j;
						blocks.add(new cars(w,h,c));
						
						ok = true;
					}//if color matches.
					
				}//for i,j in initial.
			
		}//for color c.
		
	}
	
	static boolean winNext(char[][] map) {
		for (int i = 5; i > 0; i++) {
			if (map[2][i] == '.')
				continue;
			if (map[2][i] == car)
				return true;
			else
				return false;
		}
		return false;
	}
}

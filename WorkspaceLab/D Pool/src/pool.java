import java.util.Scanner;

public class pool {
	
	static Scanner s;
	static int a, b;
	
	public static void main(String[] zzz) {
		
		s = new Scanner(System.in);
		
		a = s.nextInt();
		b = s.nextInt();
		
		int caseNumber=1;
		
		while (a != 0 && b != 0) {
			int h=solve();
			
			System.out.println("Case "+caseNumber+++": "+h);
			
			
			a = s.nextInt();
			b = s.nextInt();
		}
		
		s.close();
	}
	
	static int solve() {
		
		double minD = a;
		minD = Math.sqrt(8*minD-7)-1;
		double maxD = b;
		maxD = Math.sqrt(8*maxD-7)-1;
		
		minD/=2;
		minD++;
		maxD/=2;
		maxD--;
		
		int min = (int) Math.floor(minD);
		int max = (int) Math.ceil(maxD);
		
		int happyCases=0;
		
		for (int i = min; i <= max; i++) {
			int x=i*(i+1)/2;
			x++;
			
			if (isSquareNum(x))
				happyCases++;
			
		}
		
		
		return happyCases;
		
	}
	
	
	
	
	static boolean isSquareNum(int n){
		int min = (int) Math.sqrt(n);
		
		return min*min>=n;
	}
	
	
	
}

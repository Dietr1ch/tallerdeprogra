import java.util.ArrayList;
import java.util.Scanner;

public class vamp {
	
	static Scanner s = new Scanner(System.in);
	static int X;
	
	public static void main(String[] zxcv) {
		
		//preCalc();
		
		X = s.nextInt();
		
		while (X != 0) {
			calcXDigits();
			X = solve();
			//X = lookup();
			System.out.println(X);
			X = s.nextInt();
		}
		
		s.close();
	}
	
	static ArrayList<Integer> border = new ArrayList<Integer>();
	static ArrayList<Integer> value = new ArrayList<Integer>();
	
	static void preCalc() {
		int X = 10;
		calcXDigits();
		
		while (X < 1000000) {
			border.add(X);
			System.out.print(X + "=>");
			X = solve();
			System.out.println(X);
			value.add(X);
			X++;
			calcXDigits();
			s.nextLine();
		}
	}
	
	static int lookup() {
		int i = -1;
		for (Integer b : border) {
			i++;
			if (b > X)
				return value.get(i);
		}
		return 0;
	}
	
	static int solve() {
		
		int a = 1;
		int b = 1;
		
		while (a * b != X) {
			
			while (a * b != X && b != 0 && a <= b) {
				a++;
				b = X / a;
			}
			
			while (!isVampDecomp(a, b) && b != 0) {
				a++;
				b = X / a;
				while (a * b != X && b != 0 && a <= b) {
					a++;
					b = X / a;
				}
			}
			
			if (b == 0) {
				X++;
				calcXDigits();
				a = 1;
				b = 1;
			}
		}
		
		return X;
		
	}
	
	static int[] Xdigits;
	static void calcXDigits() {
		Xdigits = digitCount(X);
	}
	
	/*
	 * static checkX(){ Xdigits = digitCount(X); }
	 */
	
	static boolean isVampDecomp(int a, int b) {
		if (X != a * b)
			return false;
		
		return eq(Xdigits, sum(digitCount(a), digitCount(b)));
	}
	
	static boolean eq(int[] a, int[] b) {
		for (int i = 0; i < 10; i++)
			if (a[i] != b[i])
				return false;
		return true;
	}
	
	static int[] sum(int[] a, int[] b) {
		int[] r = new int[10];
		for (int i = 0; i < 10; i++)
			r[i] = a[i] + b[i];
		return r;
	}
	
	static int[] digitCount(int n) {
		int[] digitCount = new int[10];
		while (n > 0) {
			digitCount[n % 10]++;
			n /= 10;
		}
		return digitCount;
	}
	
}

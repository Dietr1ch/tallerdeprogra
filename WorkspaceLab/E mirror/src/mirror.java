import java.util.Scanner;



public class mirror {
	static Scanner s = new Scanner(System.in);
	static String line;
	static String reversible = "[bdpqiovwx]*";
	
	public static void main(String[]zzz){
		line=s.nextLine();
		while(!line.equals("#")){
			solve();			
			line=s.nextLine();
		}
		
		
		s.close();
	}
	
	
	static void solve(){
		if (line.matches(reversible)){
			line=reverse(line);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < line.length(); i++) {
				char c = line.charAt(i);
				switch (c) {
					case 'b':
						c='d';
						break;
					case 'd':
						c='b';
						break;
					case 'p':
						c='q';
						break;
					case 'q':
						c='p';
						break;
				}
				sb.append(c);
			}
			System.out.println(sb.toString());
		}
		else
			System.out.println("INVALID");
	}
	
	static String reverse(String s) {
    return new StringBuffer(s).reverse().toString();
}
}

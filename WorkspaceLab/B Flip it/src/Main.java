import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Main {
	
	static Scanner s;
	static int n, m;
	static int t, l, r, b;
	static Stack<Integer>[][] matriz;
	static String word;
	static int testCase = 1;


	public static void main(String[] asdf) {
		
		s = new Scanner(System.in);
		
		n = s.nextInt();
		m = s.nextInt();
		
		while (n > 0 && m > 0) {
			
			System.out.print("Case " + (testCase++) + ": ");
			
			t = 0;
			l = 0;
			r = m - 1;
			b = n - 1;
			matriz = new Stack[n][m];
			
			
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					matriz[i][j] = new Stack<Integer>();
					matriz[i][j].push(s.nextInt());
				}
			}
			s.nextLine();
			word = s.nextLine();
			
			solve();
			
			n = s.nextInt();
			m = s.nextInt();
		}
		
		s.close();
	}
	
	private static void solve() {
		
		int largo = word.length();
		
		for (int a = 0; a < largo; a++) {
			char c = word.charAt(a);
			
			switch (c) {
				case 'L':
					volcarCol(l++, l);
					break;
				case 'R':
					volcarCol(r--, r);
					break;
				case 'T':
					volcarFila(t++, t);
					break;
				case 'B':
					volcarFila(b--, b);
					break;
				default:
					break;
			}
			
		}
		
		Stack<Integer> st = matriz[t][l];
		String str = "";
		
		while (!st.isEmpty()) {
			int card = st.pop();
			if (card < 0)
				continue;
			
			str = card + " " + str;
		}
		
		System.out.println(str);
		
	}
	
	static void volcarFila(int origen, int dest) {
		for (int col = l; col <= r; col++)
			volcar(matriz[origen][col], matriz[dest][col]);
	}
	
	static void volcarCol(int origen, int dest) {
		for (int fil = t; fil <= b; fil++)
			volcar(matriz[fil][origen], matriz[fil][dest]);
	}
	
	static void volcar(Stack<Integer> origen, Stack<Integer> dest) {
		
		while (!origen.isEmpty())
			dest.push(-origen.pop());
		
	}
}

import java.util.Scanner;



public class mad {
	static Scanner s = new Scanner(System.in);
	
	static int k;
	static String data;
	static int[] nums;
	
	
	public static void main(String[]zzz){
		k = s.nextInt();
		
		while(k>0){
			parse();
			solve();
			k = s.nextInt();
		}
		
		
		s.close();
	}
	
	static void parse(){
		data = s.nextLine();
		String[] swNums = data.split(" ");
		nums = new int[swNums.length];
		for (int i = 1; i < nums.length; i++)
			nums[i]=Integer.parseInt(swNums[i]);
	}
	
	
	static void solve(){
		int current=0;
		int printed=0;
		StringBuilder sb = new StringBuilder();
				
		for (int i = 1; i < nums.length; i++) {
			nums[i]-=printed;
			current++;
			for (int j = 0; j < nums[i]; j++,printed++) {
				sb.append(current+" ");
			}
		}
		System.out.println(sb.toString().trim());
		
	}
}

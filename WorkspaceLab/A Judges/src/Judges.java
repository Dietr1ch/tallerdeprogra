import java.util.Scanner;

public class Judges {

	static int N, SH, SM, DH, DM; 
	
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		N=s.nextInt();
		
		for (int i = 0; i < N; i++) {		
			SH=s.nextInt();
			SM=s.nextInt();
			DH=s.nextInt();
			DM=s.nextInt();
			
			System.out.println("------+---------\n time | elapsed \n------+---------");
		
			int dura = DH;
			int it =0;
			if((SM+DM)>=60)
				dura++;
			
			int sumado=(SH+1);
			if(sumado>=12)
				sumado-=12;
			
			if(SM!=0)
			{
				if(SH!=10 && SH!=11 && SH!=12)
					System.out.println(" "+SH+":XX | XX - "+SM);
				else
					System.out.println(SH+":XX | XX - "+SM);
				
				if(dura>0)
				{
					if(sumado!=10 && sumado!=11 && sumado!=12)
						System.out.println(" "+sumado+":XX | XX + "+(60-SM));
					else
						System.out.println(sumado+":XX | XX + "+(60-SM));
				}
				
				it=2;
			}
			else
			{
				if(SH!=10 && SH!=11 && SH!=12)
					System.out.println(" "+SH+":XX | XX");
				else
					System.out.println(SH+":XX | XX");
				it=1;
			}
			
			for (int j = it; j <= dura; j++) { /////////
				int minute = (60*(j-1))+(60-SM);
				int sumado2=(SH+j);
				if(sumado2>=12)
					sumado2-=12;
				if(sumado2==0)
					sumado2=12;
				if(sumado2!=10 && sumado2!=11 && sumado2!=12)
					System.out.println(" "+sumado2+":XX | XX + "+minute);	
				else
					System.out.println(sumado2+":XX | XX + "+minute);	
			}		
		}
		
	}
	
	
	
	
}

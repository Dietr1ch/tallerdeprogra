import java.util.Scanner;



public class difference {
	
	static Scanner s = new Scanner(System.in);
	static int steps = 0;
	
	public static void main(String[] zxcv){
		 int a,b,c,d;
		 
		 a=s.nextInt();
		 b=s.nextInt();
		 c=s.nextInt();
		 d=s.nextInt();
		 while(a!=0 && b!=0 && c!=0 && d!=0){
			 System.out.println(runRec(a,b,c,d));
			 
			 a=s.nextInt();
			 b=s.nextInt();
			 c=s.nextInt();
			 d=s.nextInt();
		 }
		 
		 s.close();
	}
	
	
	static int runRec(int a, int b, int c, int d){
		steps=0;
		recSteps(a,b,c,d);
		return steps;
	}
	
	static void recSteps(int a, int b, int c, int d){
		if(a==b && b==c && c==d)
			return;
		steps++;
		int A=Math.abs(a-b);
		int B=Math.abs(b-c);
		int C=Math.abs(c-d);
		int D=Math.abs(d-a);
		
		recSteps(A,B,C,D);
	}
}

import java.awt.geom.Point2D;
import java.util.Scanner;



public class euclid {
	static Scanner s = new Scanner(System.in);
	
	static Point2D.Double A,B,C,D,E,F;
	
	public static void main(String[]zccxzv){
		
		readProblem(s);
		while(nonZero(A) || nonZero(B) || nonZero(C)){
			
			//Coords from D
			E.x-=D.x;
			E.y-=D.y;
			F.x-=D.x;
			F.y-=D.y;
			double triArea = crossProd(E,F)/2.0;// +-
			
			//Coords from A
			C.x-=A.x;
			C.y-=A.y;
			B.x-=A.x;
			B.y-=A.y;
			double parallelogramArea = crossProd(C,B); //+- 
			
			
			double alpha = Math.abs(triArea/parallelogramArea);
			
			
			//scale (from A)
			C.x*=alpha;
			C.y*=alpha;
			
			//Coords from Origin.
			C.x+=A.x;
			C.y+=A.y;
			
			double hx,hy;
			double gx,gy;
			
			hx=C.x;
			hy=C.y;
			
			//B from A
			gx=hx+B.x;
			gy=hy+B.y;
			
			//System.out.print(gx+" ");
			//System.out.print(gy+" ");
			//System.out.print(hx+" ");
			//System.out.println(hy);
			System.out.format("%.3f %.3f %.3f %.3f\n", gx,gy,hx,hy);
			
			
			readProblem(s);
		}

		s.close();
	}
	
	static boolean nonZero(Point2D.Double p){
		return p.x!=0 || p.y!=0;
	}
	
	static void readProblem(Scanner s){
		A=readP2D(s);
		B=readP2D(s);
		C=readP2D(s);
		D=readP2D(s);
		E=readP2D(s);
		F=readP2D(s);
	}
	
	static Point2D.Double readP2D(Scanner s){
		Point2D.Double p = new Point2D.Double(s.nextDouble(),s.nextDouble());
		return p;
	}
	
	static double crossProd(Point2D.Double a,Point2D.Double b){
		
		double res =0;
		
		res = a.x*b.y;
		res-= b.x*a.y;
		
		return res;
	}
	
	
}

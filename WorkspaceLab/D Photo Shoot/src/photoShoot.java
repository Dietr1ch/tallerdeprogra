import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class photoShoot {
	
	static Scanner s;
	static int N, X, Y, f;
	
	public static void main(String[] asdf) {
		s = new Scanner(System.in);
		
		N = s.nextInt();
		X = s.nextInt();
		Y = s.nextInt();
		f = s.nextInt();
		
		int caseNumber=1;
		while (N + X + Y + f != 0) {
			
			System.out.print("Case "+caseNumber+": ");
			caseNumber++;
			
			solve();
			
			N = s.nextInt();
			X = s.nextInt();
			Y = s.nextInt();
			f = s.nextInt();
		}
		
		s.close();
	}
	
	static void solve() {
		Double[] personas = new Double[N];
		
		for (int i = 0; i < N; i++) {
			int x = s.nextInt();
			int y = s.nextInt();
			
			personas[i] = Math.atan2(y - Y, x - X)+Math.PI;
			personas[i] = 180*personas[i]/Math.PI;
		}
		Arrays.sort(personas);
		
		int minFotos = N + 1;
		
		for (int personaInicial = 0; personaInicial < personas.length; personaInicial++) {
			int fotos = 0;
			int fotografiados=0;
			

			int ultimaPersona = personaInicial;
			while (fotografiados<N) {
				fotos++;
				if(fotos>minFotos){
					break;
				}
				double anguloInicialFoto = personas[ultimaPersona];
				double anguloFinalFoto = (anguloInicialFoto+f)%360;
				
				
				while (
						fotografiados<N &&
						cabe(anguloInicialFoto, anguloFinalFoto,personas[ultimaPersona])
						){
					fotografiados++;
					ultimaPersona= (ultimaPersona+1)%N;
				}
			}
			
			if(fotos<minFotos)
				minFotos=fotos;
		}
		
		System.out.println(minFotos);
	}
	
	static boolean cabe(double i, double f, double alfa){
		
		if(f>=i)//caso bonito
			return (i<= alfa && alfa<=f);
		else
			return !(f< alfa && alfa<i);
	}
}

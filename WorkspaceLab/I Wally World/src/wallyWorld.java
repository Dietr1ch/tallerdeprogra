import java.awt.geom.Line2D;
import java.text.DecimalFormat;
import java.util.Scanner;

public class wallyWorld {
	
	static Scanner s;
	
	static int x1, y1, x2, y2;
	static int wx, wy, wX, wY;
	
	public static void main(String[] zzz) {
		s = new Scanner(System.in);
		
		x1 = s.nextInt();
		y1 = s.nextInt();
		x2 = s.nextInt();
		y2 = s.nextInt();
		
		int caseNumber = 1;
		
		while (x2 != 0 && x1 != 0 && y2 != 0 && y1 != 0) {
			wx = s.nextInt();
			wy = s.nextInt();
			wX = s.nextInt();
			wY = s.nextInt();
			
			System.out.print("Case " + caseNumber++ + ": ");
			double dist = solveCase()/2;
			System.out.printf("%1$.3f", dist);
      System.out.println();
			
			x1 = s.nextInt();
			y1 = s.nextInt();
			x2 = s.nextInt();
			y2 = s.nextInt();
		}
		
		s.close();
	}
	
	
	
	
	
	
	static double solveCase() {
		
		Line2D.Double straightLine = new Line2D.Double(x1, y1, x2, y2);
		Line2D.Double wall = new Line2D.Double(wx, wy, wX, wY);
		
		if (straightLine.intersectsLine(wall))
			return shortestWallPath();
		return straightPath();
		
	}
	
	static double dist(int ax, int ay, int bx, int by) {
		int dx = (ax - bx);
		int dy = (ay - by);
		
		double sqDist = (dx * dx) + (dy * dy);
		return Math.sqrt(sqDist);
	}
	
	static double shortestWallPath() {
		double wallA = dist(x1, y1, wx, wy) + dist(wx, wy, x2, y2);
		double wallB = dist(x1, y1, wX, wY) + dist(wX, wY, x2, y2);
		
		return Math.min(wallA, wallB);
		
	}
	
	static double straightPath() {
		return dist(x1, y1, x2, y2);
	}
	
}

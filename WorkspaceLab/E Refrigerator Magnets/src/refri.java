import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;



public class refri {
	
	static Scanner s;
	static String input="";
	
	public static void main(String[] zzz) {
		s = new Scanner(System.in);
		
		input = s.nextLine();
		
		while (!input.equals("END")) {
			solve();
			input = s.nextLine();
		}
		
		s.close();
	}
	
	
	static void solve(){
		HashMap<Character, Boolean> chars = new HashMap<Character, Boolean>();
		char[] word = input.toCharArray();
		
		for (Character c : word){
			if (c==' ')
				continue;
			if(chars.containsKey(c))
				return;
			else
				chars.put(c, true);
		}
		
		System.out.println(input);
		
	}
}
